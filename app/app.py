import time, random
import prometheus_client
from flask import Flask, Response, request, make_response
from prometheus_flask_exporter import PrometheusMetrics

CONTENT_TYPE_LATEST = str('text/plain; charset=utf-8')

app = Flask(__name__)
metrics = PrometheusMetrics(app)

@app.route('/')
def home():
    sleep = float(request.args.get('sleep', 1)) / 1000
    status_code = request.args.get('status_code', 200)
    time.sleep(sleep)
    msg = "<html><strong>Hello World!</strong>" + "<br/>slept milliseconds: " + str(sleep) + "<br/>status code: " + str(status_code) +  "<br/> &copy; 2018 AboutCoders</html>"
    return Response(msg, status_code)

@app.route('/metrics')
def metrics():
    return Response(prometheus_client.generate_latest(), mimetype=CONTENT_TYPE_LATEST)

if __name__ == '__main__':
    app.run()
