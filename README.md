Prometheus Example App
----------------------

A Prometheus instrumented example application based on the Python's [Flask](http://flask.pocoo.org/) framework.

Prometheus metrics are provided using the [prometheus-flask-exporter](https://github.com/rycus86/prometheus_flask_exporter).

## Application Endpoints
The following endpoints are provided:
* <http://localhost:5000/> with the optional URL parameters:
  * `sleep`: The number of milliseconds to sleep before the HTTP response is returned (default is 1)
  * `status_code` The HTTP status code to return (default is 200)
* <http://localhost:5000/metrics>

## Docker

A Docker image is available at [datenbahn/prometheus-example-app](https://hub.docker.com/r/datenbahn/prometheus-example-app/).

## Development

### Running the application locally

````console
cd app
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
export FLASK_APP=app
flask run
````

### Running the docker container

```console
docker build -t prometheus-example-app .
docker run -p 5000:5000 \
    -t prometheus-example-app
```